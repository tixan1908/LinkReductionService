﻿// Импортируем зависимости
import Vue from 'vue'
import App from './App'
import Home from './Components/Home'
import VueRouter from 'vue-router'
import axios from 'axios'
import LoginVue from './Components/Account/Login.vue';
import RegisterVue from './Components/Account/Register.vue';
import UserVue from './Components/Account/User.vue';
import AllGroupVue from './Components/AuthLinks/AllGroup.vue';
import NotActiveVue from './Components/NotActive.vue';
import AllLinksVue from './Components/AuthLinks/AllLinks.vue';
import store from './store/store';
import ViewGroupVue from './Components/AuthLinks/ViewGroup.vue';
import NotFoundVue from './NotFound.vue';



window.axios = axios
Vue.use(VueRouter)

// Определяем роуты
const routes = [
    { path: '/', component: Home},
    { path: '/Links', component: AllLinksVue },
    { path: '/Login', component: LoginVue },
    { path: '/Register', component: RegisterVue },
    { path: '/User', component: UserVue },
    { path: '/Groups', component: AllGroupVue },
    { path: '/NotActive', component: NotActiveVue },
    { path: '/Group/:name', component: ViewGroupVue },
    { path: '*', component: NotFoundVue } 
]

// Создаем экземпляр роутера и передайте опцию `routes`
const router = new VueRouter({
    routes,
    mode: 'history'
})

var bus = new Vue()
// Создаем экземпляр vue
new Vue({
    el: '#app',
    template: '<App/>',
    data: {},
    components: {
        App, bus
    },
    router,
    store
}).$mount('#app')

