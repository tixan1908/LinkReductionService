﻿import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: {}
    },
    getters: {
        getUser: state => state.user 
    },
    actions: {
        loadUser(context, user) {
            axios.get('api/Account/GetUserName')
                .then(response => { context.commit('loadUser', response.data); })
        }
    },
    mutations: {
        loadUser(state, payload) {
            state.user = payload;
        }
    }
})