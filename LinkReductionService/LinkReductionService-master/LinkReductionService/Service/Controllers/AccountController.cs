﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Service.Models;
using Service.Models.VK;
using Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Service.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;


        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        private User GetUser()
        {
            User user = _userManager.Users.FirstOrDefault(x => x.Email.Equals(User.Identity.Name));
            return user;
        }


        [HttpPost("[action]")]
        public async Task<JsonResult> Register([FromBody]RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Error = "";
                User user = new User { Email = model.Email, UserName = model.Email, Name = model.Name, Role = "user" };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, true);
                    model.ReturnUrl = "/";
                    return Json(model);
                }
                else
                {
                    foreach (var error in result.Errors)
                        model.Error = error.Description + "\n";
                    return Json(model);
                }
            }
            model.Error = "Invalid data";
            return Json(model);
        }

        [HttpGet("[action]")]
        public JsonResult GetUserName()
        {
            User user = GetUser();
            return Json(user);
        }

        [HttpPost("[action]")]
        public async Task<JsonResult> Login([FromBody]LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Error = "";
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Json(model);
                    }
                    else
                    {
                        model.ReturnUrl = "/";
                        return Json(model);
                    }
                }
                else
                {
                    model.Error = "Incorrect login and/or password";
                    return Json(model);
                }
            }
            model.Error = "Invalid data";
            return Json(model);
        }

        [HttpPost("[action]")]
        public async Task<JsonResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            return Json("LogOut");
        }

        [HttpPost("[action]")]
        public async Task<JsonResult> ChangePassword([FromBody]ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = _userManager.Users.FirstOrDefault(x => x.Email.Equals(User.Identity.Name));
                if (user != null)
                {
                    IdentityResult result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return Json("Password successfully changed");
                    }
                    else
                    {
                        string answer = "";
                        foreach (var err in result.Errors)
                        {
                            answer = err.Description + "\n";
                        }
                        return Json(answer);
                    }
                }
                else
                {
                    return Json("User not found");
                }
            }
            return Json("Incorrect data");
        }

        private string clientId = "6604445";
        private string clientSecret = "ySWyaiGp9ZFrNyjwo5ET";



        [HttpGet("[action]")]
        public async Task<RedirectResult> VKLoginCallback(string code)
        {

            if (code != null)
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage tokenRes = await client.GetAsync("https://oauth.vk.com/access_token?client_id=" + clientId + "&client_secret=" + clientSecret + "&redirect_uri=http://localhost:62283/api/Account/VKLoginCallback&code=" + code);
                string tokenResponse = await tokenRes.Content.ReadAsStringAsync();
                VkTokenResponse vkTokenResponse = JsonConvert.DeserializeObject<VkTokenResponse>(tokenResponse);

                if (vkTokenResponse.Error == null)
                {
                    HttpResponseMessage userResult = await client.GetAsync(
                        "https://api.vk.com/method/users.get?access_token=" + vkTokenResponse.AccessToken +"&v=5.78");
                    string userResponse = await userResult.Content.ReadAsStringAsync();
                    VkUser vkUser = JsonConvert.DeserializeObject<VkUser>(userResponse);

                    if (vkUser != null)
                    {
                        string userEmail;
                        if (vkTokenResponse.Email == null)
                            userEmail = "FastLink" +vkUser.Response[0].ID + "@noEmail.ru";
                        else
                            userEmail = vkTokenResponse.Email;

                        var result = await _signInManager.PasswordSignInAsync(userEmail, "Pas" + vkUser.Response[0].ID + "word", true, false);

                        if (result.Succeeded)
                        {
                            return Redirect("/");
                        }
                        else
                        {
                            User user = new User { Email = userEmail, UserName = userEmail, Name = vkUser.Response[0].FirstName + ' ' + vkUser.Response[0].LastName, Role = "user" };
                            var result2 = await _userManager.CreateAsync(user, "Pas" + vkUser.Response[0].ID + "word");
                            if (result2.Succeeded)
                            {
                                await _signInManager.SignInAsync(user, true);
                                return Redirect("/");
                            }
                        }
                    }

                } 

            }
            return Redirect("/Login");
        }
    }
}
