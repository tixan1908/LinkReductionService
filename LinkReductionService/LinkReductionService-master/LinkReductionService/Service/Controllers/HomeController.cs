﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Models;
using Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;

namespace Service.Controllers
{
    
    public class HomeController : Controller
    {
        ApplicationContext db;

        public HomeController(ApplicationContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            Group group = db.Groups.FirstOrDefault(x => x.Name == "Undefined");
            if (group == null)
            {
                db.Groups.Add(new Group { Name = "Undefined", UserName = "ADMIN", UserID = "0" });
                db.SaveChanges();
            }
            return View();
            
        }
        

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [Route("0_{id}")]
        public async Task<RedirectResult> _Redirect(string id)
        {
            string url = Constants.DomainName + "/0_" + id;
            var link = await db.Links.FirstOrDefaultAsync(x => x.UrlFinite == url);
            if (link.IsActive)
            {
                var group = await db.Groups.FirstOrDefaultAsync(x => x.ID == link.GroupID);
                link.CountConversions++;
                group.CountConvers++;
                db.Links.Update(link);
                db.Groups.Update(group);
                await db.SaveChangesAsync();
                return Redirect(link.UrlInitial);
            }
            return Redirect(Constants.DomainName + "/NotActive");
        }
    }
}
