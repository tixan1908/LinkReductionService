﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Service.Models;
using Service.ViewModels;

namespace Service.Controllers
{
    [Route("api/[controller]")]
    public class LinksController : Controller
    {
        ApplicationContext db;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public LinksController(ApplicationContext context, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            db = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet("[action]")]
        public JsonResult GetAllLinks()
        {
            User user = GetUser();
            if (user != null)
                if (user.Role == "admin")
                    return Json(db.Links.ToList());
                else
                    return Json(db.Links.Where(x => x.UserID == user.Id).ToList());
            return Json("User is not authorized");
        }
        

        [HttpGet("[action]/{name}")]
        public JsonResult GetGroupLinks(string name)
        {
            User user = GetUser();
            if (user != null)
                    return Json(
                        db.Links
                        .Where(x => x.GroupName == name && (x.UserID == user.Id || user.Role == "admin"))
                        .ToList()
                    );
            return Json("User is not authorized");
        }



        [HttpPost("[action]")]
        public async Task<JsonResult> AddLink([FromBody]LinkUrl linkUrl)
        {
            if (ModelState.IsValid)
            {
                User user = GetUser();
                string userId = "0";
                string userName = "Unregistered";
                if (user != null)
                {
                    userId = user.Id;
                    userName = user.Name;
                }
                Link link = await db.Links.FirstOrDefaultAsync(x => (x.UrlInitial == linkUrl.Url) && (x.UserID == userId));
                if (link == null)
                {
                    link = new Link { UrlInitial = linkUrl.Url, UserID = userId, IsActive=true};
                    await db.Links.AddAsync(link);  
                    await db.SaveChangesAsync();
                    link = await db.Links.FirstOrDefaultAsync(x => (x.UrlInitial == linkUrl.Url) && (x.UserID == userId));
                    link.UrlFinite = Constants.DomainName + "/0_" + link.ID;
                    Group group = await db.Groups.FirstOrDefaultAsync(x => x.ID == linkUrl.GroupId);
                    if (group == null)
                    {
                        group = await db.Groups.FirstOrDefaultAsync(x => x.Name == "Undefined" && x.UserName == "ADMIN");
                    }
                    link.UserName = userName;
                    link.GroupName = group.Name;
                    link.GroupID = group.ID;
                    group.CountLinks++;
                    db.Groups.Update(group);
                    db.Links.Update(link);
                    await db.SaveChangesAsync();
                    return Json(link.UrlFinite);
                }
                else return Json(link.UrlFinite);
            }
            return Json("String is empty!");
        }

        [HttpPost("[action]")]
        public async Task<JsonResult> UpdateLink([FromBody] Link link)
        {
            Link l = await db.Links.FirstOrDefaultAsync(x => x.ID == link.ID);
            l.IsActive = link.IsActive;
            db.Update(l);
            await db.SaveChangesAsync();
            return Json("Updated");
        }

        [HttpGet("[action]")]
        public JsonResult GetGroups()
        {
            User user = GetUser();
            if (user != null)
                if (user.Role == "admin")
                    return Json(db.Groups.ToList());
                else
                    return Json( db.Groups.Where(x => x.UserID == user.Id).ToList());
            else
                return Json("User is not authorized");
        }

        [HttpGet("[action]/{name}")]
        public async Task<JsonResult> GetGroup(string name)
        {
            User user = GetUser();
            if (user != null)
            {
                Group group;
                if (user.Role == "admin")
                {
                    group = await db.Groups.FirstOrDefaultAsync(x => x.Name == name);
                }
                else
                {
                    if (name == "Undefined")
                        group = await db.Groups.FirstOrDefaultAsync(x => x.Name == name);
                    else
                        group = await db.Groups.FirstOrDefaultAsync(x => x.Name == name && x.UserID == user.Id);
                }
                if (group != null)
                    return Json(group);
                else
                    return Json("This is not your group");
                
            }
            return Json("User is not authorized");
        }

        [HttpPost("[action]")]
        public async Task<JsonResult> AddGroup([FromBody]ViewAddGroup new_group)
        {
            if (ModelState.IsValid && new_group.nameGroup != "Undefined")
            {
                User user = GetUser();
                if (user != null)
                {
                    Group group = new Group
                    {
                        Name = new_group.nameGroup,
                        UserName = user.Name,
                        UserID = user.Id
                    };
                    db.Groups.Add(group);
                    await db.SaveChangesAsync();
                    return Json(group);
                }
                return Json(null);
            }
            return Json(null);
        }

        [HttpDelete("[action]/{id}")]
        public async Task<JsonResult> DeleteGroup(int id)
        {
            User user = GetUser();
            if (user != null)
            {
                Group group = await db.Groups.FirstOrDefaultAsync(x => (x.ID == id) && (x.UserID == user.Id || user.Role == "admin"));
                if (group != null)
                {
                    List<Link> delLink = db.Links.Where(x => x.GroupID == group.ID).ToList();
                    foreach (var item in delLink)
                    {
                        db.Links.Remove(item);
                    }
                    db.Groups.Remove(group);
                    await db.SaveChangesAsync();
                    return Json("Group deleted");
                }
                return Json("Group not found");
            }
            return Json("User is not authorized");
        }

        [HttpDelete("[action]/{id}")]
        public async Task<JsonResult> DeleteLink(int id)
        {
            User user = GetUser();
            if (user != null)
            {
                Link link = await db.Links.FirstOrDefaultAsync(x => (x.ID == id) && (x.UserID == user.Id || user.Role == "admin"));
                if (link != null)
                {
                    Group group = await db.Groups.FirstOrDefaultAsync(x => x.ID == link.GroupID);
                    if (group != null)
                    {
                        group.CountLinks--;
                        group.CountConvers -= link.CountConversions;
                        db.Groups.Update(group);
                    }
                    db.Links.Remove(link);
                    await db.SaveChangesAsync();
                    return Json("Link deleted");
                }
                return Json("Link not found");
            }
            return Json("User is not authorized");
        }

        [HttpPost("[action]")]
        public async Task<JsonResult> RenameGroup([FromBody]Group newGroup)
        {
            User user = GetUser();
            if (user != null)
            {
                Group group = await db.Groups.FirstOrDefaultAsync(x => (x.ID == newGroup.ID) && (x.UserID == user.Id || user.Role == "admin"));
                if (group != null)
                {
                    group.Name = newGroup.Name;
                    db.Groups.Update(group);
                    await db.SaveChangesAsync();
                    return Json("Group renamed");
                }
                return Json("Group not found");
            }
            return Json("User is not authorized");
        }

        private User GetUser()
        {
            User user = _userManager.Users.FirstOrDefault(x => x.Email.Equals(User.Identity.Name));
            return user;
        }


    }


}
