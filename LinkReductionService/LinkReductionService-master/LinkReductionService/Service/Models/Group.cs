﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Models
{
    public class Group
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string UserID { get; set; }
        public int CountConvers { get; set; }
        public string UserName { get; set; }
        public int CountLinks { get; set; }
    }
}
