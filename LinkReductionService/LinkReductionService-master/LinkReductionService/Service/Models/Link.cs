﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Models
{
    public class Link
    {
        public int ID { get; set; }
        public string UrlInitial { get; set; }
        public string UrlFinite { get; set; }
        public bool IsActive { get; set; }
        public int Type { get; set; }
        public int CountConversions { get; set; }

        public string UserID { get; set; }
        public string UserName { get; set; }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
    }
}
