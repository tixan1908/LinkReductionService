﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Models.VK
{
    public class VkUser
    {
        [JsonProperty("response")]
        public VKResponse[] Response { get; set; }

        public class VKResponse
        {
            [JsonProperty("first_name")]
            public string FirstName { get; set; }

            [JsonProperty("last_name")]
            public string LastName { get; set; }

            [JsonProperty("id")]
            public int ID { get; set; }
        }
    }
}
