﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Service.ViewModels
{
    public class LinkUrl
    {
        [Required]
        public string Url { get; set; }

        [Required]
        public int GroupId { get; set; }
    }
}
