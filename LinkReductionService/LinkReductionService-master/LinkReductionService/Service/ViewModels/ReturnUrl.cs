﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.ViewModels
{
    public class ReturnUrl
    {
        public string Url { get; set; }
    }
}
