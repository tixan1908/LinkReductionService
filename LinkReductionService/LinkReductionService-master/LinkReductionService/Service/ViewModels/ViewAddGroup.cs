﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Service.ViewModels
{
    public class ViewAddGroup
    {
        [Required]
        public string nameGroup { get; set; }
    }
}
